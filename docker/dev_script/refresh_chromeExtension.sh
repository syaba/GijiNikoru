#!/usr/bin/env bash

### Chromeプラグインモジュール更新スクリプト
# front/src 配下のソースを変更した後、このスクリプトを実行することで、front/ChromeExtension を更新することができます。

docker-compose exec front yarn install --pure-lockfile
docker-compose exec front npm run lint
docker-compose exec front npm run bundle
