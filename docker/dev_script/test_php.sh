#!/usr/bin/env bash

### Chromeプラグインモジュール更新スクリプト
# front/src 配下のソースを変更した後、このスクリプトを実行することで、front/ChromeExtension を更新することができます。

docker-compose exec gn-server /usr/local/work/vendor/bin/phpunit --tap /usr/local/work/tests/
