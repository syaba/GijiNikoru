SELECT
  n.movie_id        AS movie_id,
  n.comment_id      AS comment_id,
  n.commented_point AS commented_point,
  n.commented_at    AS commented_at
FROM
  nikorare n
  INNER JOIN
  (
    SELECT
      max(movie_id) AS movie_id,
      count(1)      AS cnt
    FROM
      (
        SELECT
          n.movie_id   AS movie_id,
          n.comment_id AS comment_id
        FROM nikorare n
        WHERE NOT EXISTS(
            SELECT *
            FROM comments c
            WHERE c.video_id = n.movie_id
                  AND c.id = n.comment_id
        )
      ) incomplete_comment_info
    GROUP BY movie_id
    ORDER BY cnt DESC
    LIMIT 1
  ) incomplete_comment_info_max
    ON n.movie_id = incomplete_comment_info_max.movie_id
WHERE NOT EXISTS(
    SELECT *
    FROM comments c
    WHERE c.id = n.comment_id)
ORDER BY commented_at DESC;
