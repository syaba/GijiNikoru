SELECT
  c.video_id,
  c.text,
  n.nikorare,
  c.posted_at,
  v.title,
  v.thumbnail,
  v.watch_url
FROM
  nikorare n
  INNER JOIN
  comments c
    ON n.movie_id = c.video_id AND
       n.comment_id = c.id
  INNER JOIN
  videos v
    ON n.movie_id = v.id
WHERE
  c.posted_by = ?

