<?php
/**
 * niconico API から正規の動画情報を取得するモジュールです。
 *
 * 実行することで、ニコられがあり、かつ、正規情報が得られていない動画のうち、いずれか1個の正規情報を取得します。
 */

require_once(__DIR__ . "/../utils.php");
require_once(__DIR__ . "/../nikorareDAO.class.php");
require_once(__DIR__ . "/../niconicoAPIUtil.class.php");
require_once(__DIR__ . "/../videosDAO.class.php");
require_once(__DIR__ . "/../jobLogsDAO.class.php");

try {
    // ジョブログ取得
    $jl_dao = new JobLogsDAO();
    $job_logs = $jl_dao->find_video_job_log();
    if ($job_logs && $job_logs['status'] == JobLogStatus::RUNNING) {
        echo('前回実行分が終了していないため、処理を開始できません。');
        return;
    }

    // 前回の実行が終了してから 10 秒経過していない場合、 10 秒待機してから最初の API アクセスを行う。
    $span = time() - $job_logs['updated_at'];
    if ($span < 10) {
        sleep(10 - $span);
    }

    // 正規情報を持っていない動画のIDを1つ取得する
    $dao = new NikorareDAO();
    $movie_id = $dao->getIncompleteVideoInfo();
    if ($movie_id === null) {
        echo('補完情報を持っていない動画が見つかりませんでした。');
        return;
    }

    // ジョブ実行テーブルに開始ログを記録する
    $jl_dao->start_video_job();
    echo("$movie_id の動画情報を取得します。");

    // 正規の動画情報を取得する
    $video_info = VideoInfo::getVideoInfo($movie_id);
    $api_connector = new NiconicoAPIConnector();
    $video_api_info = $api_connector->getVideoAPIInfo($movie_id);
    if (!$video_info || !$video_api_info) {
        // 異常終了を記録する
        echo('APIからの動画情報の取得に失敗しました。');
        $jl_dao->abort_video_job();
        return;
    }

    // 取得した補完情報をDBに登録する
    $video_dao = new VideosDAO();
    $success = $video_dao->create($video_api_info, $video_info);
    if (!$success) {
        // 異常終了を記録する
        echo('動画情報の登録に失敗しました。');
        $jl_dao->abort_video_job();
        return;
    }

    // 正常終了を記録する
    $jl_dao->done_video_job();

} catch (Exception $e) {
    // 異常終了を記録する
    echo('処理エラーが発生しました。');
    $jl_dao->abort_video_job();
    return;
}
