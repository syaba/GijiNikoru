<?php
/**
 * ジョブログをメール通知するモジュールです。
 *
 * 各種ジョブの実行結果であるジョブログを取得し、設定されたメールアドレスへとメール通知します。
 */

require_once(__DIR__ . "/../utils.php");
require_once(__DIR__ . "/../nikorareDAO.class.php");
require_once(__DIR__ . "/../niconicoAPIUtil.class.php");
require_once(__DIR__ . "/../videosDAO.class.php");
require_once(__DIR__ . "/../jobLogsDAO.class.php");

try {
    // ジョブログ取得
    $jl_dao = new JobLogsDAO();

    // 動画ジョブログ取得
    $video_job_logs = $jl_dao->find_video_job_log();
    $video_status = JobLogStatus::get_status_string($video_job_logs['status']);
    $video_updated_at = date('Y/m/d H:i:s', $video_job_logs['updated_at']);

    // コメントジョブログ取得
    $comment_job_logs = $jl_dao->find_comment_job_log();
    $comment_status = JobLogStatus::get_status_string($comment_job_logs['status']);
    $comment_updated_at = date('Y/m/d H:i:s', $comment_job_logs['updated_at']);

    // メールボディ作成
    $body = <<<BODY
Video job log
  last_status: $video_status
  last_updated_at: $video_updated_at
Comment job log
  last_status: $comment_status
  last_updated_at: $comment_updated_at
BODY;

    // メール送信
    send_mail('GijiNikoru: Job log', $body);

} catch (Exception $e) {
    // 異常終了を通知する
    send_mail('GijiNikoru: Failed to get job log', 'No body.');
    return;
}
