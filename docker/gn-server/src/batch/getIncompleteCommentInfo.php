<?php
/**
 * niconico API から正規のコメント情報を取得するモジュールです。
 *
 * 実行することで、ニコられがあり、かつ、正規情報が得られていないコメントのうち、いずれか1個の正規情報を取得します。
 */

require_once(__DIR__ . "/../utils.php");
require_once(__DIR__ . "/../nikorareDAO.class.php");
require_once(__DIR__ . "/../niconicoAPIUtil.class.php");
require_once(__DIR__ . "/../videosDAO.class.php");
require_once(__DIR__ . "/../commentsDAO.class.php");
require_once(__DIR__ . "/../jobLogsDAO.class.php");

try {
    // ジョブログ取得
    $jl_dao = new JobLogsDAO();
    $job_logs = $jl_dao->find_comment_job_log();
    if ($job_logs && $job_logs['status'] == JobLogStatus::RUNNING) {
        echo('前回実行分が終了していないため、処理を開始できません。');
        return;
    }

    $span = time() - $job_logs['updated_at'];
    if ($span < 10) {
        // 前回の実行が終了してから 10秒経過していない場合、10秒待機してから最初の API アクセスを行う。
        sleep(10 - $span);
    }

    $dao = new NikorareDAO();
    $incomplete_comment_info = $dao->getIncompleteCommentInfo();
    $started_at = time();
    $comment_dao = new CommentsDAO();

    if (!$incomplete_comment_info) {
        echo('補完情報を持っていないコメントが見つかりませんでした。');
        return;
    }

    // ジョブ実行テーブルに開始ログを記録する
    $jl_dao->start_comment_job();

    $video_id = $incomplete_comment_info[0]['movie_id'];
    echo("$video_id のコメント情報を取得します。");
    ?>

    <ul>
        <?php foreach ($incomplete_comment_info as $ici): ?>
            <li><?= $ici['comment_id'] ?></li>
        <?php endforeach ?>
    </ul>

    <?php

    // まずは最新のコメントを取得する
    $api_connector = new NiconicoAPIConnector();

    $comments = $api_connector->getComments($video_id, null, null);

    // 取得完了コメントのIDリスト
    $completed_comment_ids = [];

    foreach ($incomplete_comment_info as $ici) {
        foreach ($comments->comments as $comment) {
            /* @var $comment Comment */
            if ($comment->id == $ici['comment_id']) {
                // 補完情報テーブルへの登録
                $comment_dao->create($video_id, $comment);
                $completed_comment_ids[] = $comment->id;
                break;
            }
        }
    }

    $additional_try_count = 0;

    if (count($incomplete_comment_info) != count($completed_comment_ids)) {

        // 未取得のコメントがあれば、コメントの投稿ポイント・投稿日をヒントとして、1コメントずつピンポイント検索する
        foreach ($incomplete_comment_info as $ici) {

            $comment_id = $ici['comment_id'];
            if (in_array($comment_id, $completed_comment_ids)) {
                // 補完済みのものをスキップ
                continue;
            }

            $additional_try_count++;

            // APIアクセス間隔を保持するため、10秒停止
            sleep(10);

            // コメント取得
            $comments = $api_connector->getComments($video_id, strtotime($ici['commented_at']), $ici['commented_point']);

            foreach ($comments->comments as $comment) {
                /* @var $comment Comment */

                // 複数の未取得コメントが一度に取得される場合を考慮し、未取得のコメント ID 全てと照合する
                foreach ($incomplete_comment_info as $ici2) {

                    if (in_array($comment_id, $completed_comment_ids)) {
                        // 補完済みのものをスキップ
                        continue;
                    }

                    if ($comment->id == $ici2["comment_id"]) {
                        // 補完情報テーブルへの登録
                        $comment_dao->create($video_id, $comment);
                        $completed_comment_ids[] = $comment->id;
                        break;
                    }
                }
            }
        }
    }

    // 正常終了を記録する
    $jl_dao->done_comment_job();

} catch (Exception $e) {
    echo '処理エラーが発生しました。';

    // 異常終了を記録する
    $jl_dao->abort_comment_job();
    return;
}

?>

    検索が終了しました。
    <br/>以下のコメントの情報が見つかりました。

    <ul>
        <?php foreach ($incomplete_comment_info as $ici): ?>
            <?php if (in_array($ici['comment_id'], $completed_comment_ids)): ?>
                <li><?= $ici['comment_id'] ?></li>
            <?php endif ?>
        <?php endforeach ?>
    </ul>

    見つかった情報は登録されました。
    <br/>所要時間は<?= time() - $started_at ?> 秒でした。
    <br/>そのうち、API接続間隔保持のための待機時間は<?= $additional_try_count * 10 ?> 秒でした。
    <br/>API接続は、<?= $additional_try_count + 1 ?> 回実行されました。

<?php
