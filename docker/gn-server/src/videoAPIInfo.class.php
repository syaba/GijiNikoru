<?php

/**
 * Class VideoAPIInfo niconico API用動画情報クラス
 */
class VideoAPIInfo
{
    public $video_id;
    public $thread_id;
    public $user_id;
    public $comment_server_url;
    public $comment_server_url_json;
    public $posted_at;

    public function __construct($video_id, $video_info_object)
    {
        $this->video_id = $video_id;
        $this->thread_id = $video_info_object["thread_id"];
        $this->user_id = $video_info_object["user_id"];
        $this->comment_server_url = $video_info_object["ms"];
        $this->comment_server_url_json = str_replace("/api/", "/api.json/", $this->comment_server_url);
        $this->posted_at = date("Y-m-d H:i:s", $this->thread_id);
    }
}
