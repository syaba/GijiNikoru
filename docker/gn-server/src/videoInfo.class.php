<?php

/**
 * Class VideoInfo niconico 用動画情報クラス
 */
class VideoInfo
{
    public $video_id; // 動画ID
    public $title; // 動画タイトル
    public $thumbnail_url; // 動画サムネイルURL
    public $length; // 動画時間（mm:ss）
    public $view_count; // 再生数
    public $comment_count; // コメント数
    public $mylist_count; // マイリスト数
    public $watch_url; // 視聴URL
    public $tags; // タグ
    public $author_user_id; // 投稿者ユーザーID
    public $author_nickname; // 投稿者名
    public $author_icon_url; // 投稿者アイコンURL

    public function __construct($video_info_object)
    {
        $this->video_id = $video_info_object["video_id"];
        $this->title = $video_info_object["title"];
        $this->thumbnail_url = $video_info_object["thumbnail_url"];
        $this->length = $video_info_object["length"];
        $this->view_count = $video_info_object["view_counter"];
        $this->comment_count = $video_info_object["comment_num"];
        $this->mylist_count = $video_info_object["mylist_counter"];
        $this->watch_url = $video_info_object["watch_url"];
        $this->tags = $video_info_object["tags"];
        $this->author_user_id = $video_info_object["user_id"];
        $this->author_nickname = $video_info_object["user_nickname"];
        $this->author_icon_url = $video_info_object["user_icon_url"];
    }

    /**
     * 動画情報を取得する
     * @param $video_id string 動画ID
     * @return VideoInfo 動画情報（検索エラーの場合、null）
     */
    public static function getVideoInfo($video_id)
    {
        $api_res = file_get_contents("http://ext.nicovideo.jp/api/getthumbinfo/" . $video_id);
        $parsed_res = simplexml_load_string($api_res);
        if (!$parsed_res) return null;
        $video_info = json_decode( json_encode( $parsed_res ), TRUE );
        $video_info_thumb = $video_info["thumb"];
        if (!$video_info_thumb) return null;
        return new self($video_info_thumb);
    }
}
