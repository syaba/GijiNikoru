<?php
/**
 * ニコられ情報表示テスト用ページ
 */

require_once(__DIR__ . "/../../utils.php");
require_once(__DIR__ . "/../../nikorareDAO.class.php");

function get_font_size($str)
{
    // 文字数を取得する
    $length = iconv_strlen($str, 'UTF-8');

    if ($length <= 12) {
        return "2.5em";
    }
    if ($length <= 25) {
        return "1.8em";
    }
    return "1.2em";
}

// ユーザID を取得する
$user_id = $_GET['user_id'];

// ユーザIDが不正な場合は終了
if (empty($user_id)) {
    echo("ユーザIDが指定されていません。ニコられ情報の取得に失敗しました。");
    return;
}

// ニコられ情報を取得する
$dao = new NikorareDAO();
$data = $dao->getNikorareInfo($user_id);

// 描画用データを作成する
$view = "";
$count = 0;


foreach ($data as $item) {

    // 視聴URL
    $watch_url = h($item["watch_url"]);
    if (!preg_match('/^https?:\/\/www\.nicovideo\.jp\//', $watch_url)) {
        // 不正データは表示しない
        continue;
    }

    // サムネイル
    $thumbnail = h($item["thumbnail"]);

    // タイトル
    $title = h($item["title"]);

    // コメント
    $text = h($item["text"]);

    // ニコられ
    $nikorare = h($item["nikorare"]);

    // フォントサイズ
    $font_size = get_font_size($text);

    $view .= <<<BODY
<li class="nikoru_item">
    <div class="nikoru_main_pane" id="nikoru_main_pane__$count" style="height: 70px;">
        <div class="comment" style="font-size: $font_size;">
            $text
        </div>
        <div class="nikorare_count">
            <span style="font-size: 1.8em;">$nikorare</span>
            <br/>ニコられ
        </div>
    </div>
    <div style="clear: both;"></div>
    <div style="width: 100%; text-align: center;">
        <span style="display: none;" id="up-arrow__$count">▲</span>
        <span id="down-arrow__$count">▼</span>
    </div>
    <div id="nikoru_sub_pane__$count" style="width: 100%; height: 50px; display: none;">
        <div style="float: left;">
            <a target="_blank" href="$watch_url"><img src="$thumbnail" class="nikoru_thumbnail"/></a>
        </div>
        <div style="margin-top: 8px; margin-left: 10px; float: left; vertical-align: middle;">
            <a target="_blank" href="$watch_url">$title</a>
        </div>
    </div>
</li>
BODY;

    $count++;
}

if ($count === 0) {
    echo("ニコられ情報が見つかりませんでした。");
}
?>
    <html class="font_family__basic">
    <head>
        <link rel="stylesheet" href="../../tools/toolPageStyle.css">
    </head>

    <body>

    <ul style="margin-top: 30px; list-style-type: none;">
        <?= $view ?>
    </ul>

    <script>
        function init() {
            const nikoruMainPanes = document.getElementsByClassName("nikoru_main_pane");
            for (let i = 0, len = nikoruMainPanes.length; i < len; i++) {
                const item = nikoruMainPanes[i];
                item.addEventListener('click', function () {
                    toggleSubPane(i);
                }, false);
            }
        }

        function toggleSubPane(index) {
            const subPane = document.getElementById(`nikoru_sub_pane__${index}`);
            const downArrow = document.getElementById(`down-arrow__${index}`);
            const upArrow = document.getElementById(`up-arrow__${index}`);
            if (subPane.style.display === "none") {
                // サブペイン表示
                subPane.style.display = "block";
                downArrow.style.display = "none";
                upArrow.style.display = "block";
                return;
            }
            // サブペイン非表示
            subPane.style.display = "none";
            downArrow.style.display = "block";
            upArrow.style.display = "none";
        }
        init();
    </script>
    </body>
    </html>

<?php
