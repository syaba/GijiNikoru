<?php
/**
 * ニコる
 */

require_once(__DIR__ . "/../../utils.php");

setHeader();

$video_id = $_POST['video_id'];
$comment_id = $_POST['comment_id'];
$commented_at = $_POST['commented_at'];
$commented_point = $_POST['commented_point'];

// TODO: 更新日時とユーザID も記録する

echo json_encode(nicoru($video_id, $comment_id, $commented_at, $commented_point));
