<?php
/**
 * ニコる情報取得
 */

require_once(__DIR__ . "/../../utils.php");

setHeader();

// リクエストから動画IDを取得する
$mid = $_GET['a'];

// NIMJをDBから取得して返却する
echo compressResponseIfCan(json_encode(getNimj($mid)));
