<?php

require_once(__DIR__ . "/comment.class.php");

/**
 * Class Comments niconico コメントAPI返却値クラス
 */
class Comments
{
    /**
     * @var VideoInfo 動画情報
     */
    public $video_info;

    /**
     * @var string （検索条件）コメント投稿日
     */
    public $commented_at;

    /**
     * @var string （検索条件）コメント投稿の動画内時点
     */
    public $commented_point;

    /**
     * @var array コメント情報配列
     */
    public $comments;

    /**
     * @var int 取得したコメント数
     */
    public $counts;

    /**
     * @var int 最小コメントID
     */
    public $min_comment_id;

    /**
     * @var int 最大コメントID
     */
    public $max_comment_id;

    /**
     * @var int スレッド
     */
    public $thread;

    /**
     * @var string チケット
     */
    public $ticket;


    public function __construct($condition, $api_result)
    {
        if (!$api_result) {
            throw new Exception("コメントの取得に失敗しました。");
        }

        $this->video_info = $condition["video_info"];
        $this->commented_at = $condition["commented_at"];
        $this->commented_point = $condition["commented_point"];
        $comments = [];
        $count = 0;

        // スレッド情報の取得
        $thread = $api_result[0]["thread"];
        $this->thread = $thread["thread"];
        $this->ticket = $thread["ticket"];

        foreach ($api_result as $ar) {
            $chat = $ar["chat"];
            if (!$chat) continue;
            $count++;
            $comments[] = new Comment($chat);
        }
        if ($comments) {
            $this->min_comment_id = $comments[0]->id;
            $this->max_comment_id = end($comments)->id;
            reset($comments);
        }
        $this->comments = $comments;
        $this->count = $count;
    }
}
