<?php

/**
 * Class Comment niconico コメントAPI返却値中のコメント1件を表すクラス
 */
class Comment
{
    /**
     * @var string コメントID
     */
    public $id;

    /**
     * @var DateTime コメント投稿日時
     */
    public $posted_at;

    /**
     * @var string コメントが投稿された動画内時点
     */
    public $point;

    /**
     * @var string コメント投稿者ユーザID
     */
    public $posted_by;

    /**
     * @var int 旧ニコられ数
     */
    public $old_nikorare;

    /**
     * @var bool 削除されたか
     */
    public $was_deleted;

    /**
     * @var string コメント内容
     */
    public $text;

    /**
     * @var string スレッド
     */
    public $thread;

    /**
     * @var string コマンド
     */
    public $mail;

    public function __construct($comment_object)
    {
        $this->id = $comment_object["no"];
        $this->thread = $comment_object["thread"];
        if (array_key_exists("mail", $comment_object)) {
            $this->mail = $comment_object["mail"];
        } else {
            $this->mail = "";
        }
        $this->posted_by = $comment_object["user_id"];
        $this->posted_at = date("Y/m/d H:i:s", $comment_object["date"]);
        $this->vpos = $comment_object["vpos"];
        $this->point = s2h($this->vpos / 100);
        $old_nikorare = $comment_object["nicoru"];
        if (is_null($old_nikorare)) {
            $old_nikorare = 0;
        }
        $this->old_nikorare = (int)$old_nikorare;
        $this->was_deleted = (bool)$comment_object["deleted"];
        if ($this->was_deleted) {
            $this->text = "このコメントは削除されました";
        } else {
            $this->text = htmlspecialchars($comment_object["content"]);
        }
    }
}
