<?php

require_once(__DIR__ . "/utils.php");
require_once(__DIR__ . "/dbUtils.php");

/**
 * Class JobLogStatus ジョブ実行結果ステータス
 */
class JobLogStatus
{
    const ABORTED = -1;  // 異常終了
    const RUNNING = 0;  // 実行中
    const DONE = 1;  // 正常終了

    /**
     * ステータスの文字列表現を取得します。
     * @param $status int ステータス
     * @return string ステータスの文字列表現
     * @throws Exception 想定外のステータスの場合、例外を投げます。
     */
    static function get_status_string($status)
    {
        if ($status == JobLogStatus::ABORTED) {
            return 'Aborted';
        }
        if ($status == JobLogStatus::RUNNING) {
            return 'Running';
        }
        if ($status == JobLogStatus::DONE) {
            return 'Done';
        }
        throw new Exception('Irregal status');
    }
}

/**
 * Class JobLogType ジョブログの種別
 */
class JobLogType
{
    const VIDEO = 0;  // 動画の正規情報取得処理
    const COMMENT = 1;  // コメントの正規情報取得処理
}

/**
 * Class JobLogsDAO ジョブログ用 DAO クラス
 */
class JobLogsDAO
{
    // DB接続
    private $conn;

    public function __construct()
    {
        // 接続する
        try {
            $this->conn = getDatabaseConnection();
        } catch (PDOException $e) {
            return ["error" => "db_connect_failed", "description" => $e->getMessage()];
        }
    }

    /**
     * ジョブログを更新します。
     * @param $type int ジョブログ種別
     * @param $status int ジョブログステータス
     * @return bool 処理が成功した場合、true。そうでなければfalse。
     */
    public function update($type, $status)
    {
        $stmt = $this->conn->prepare(<<<SQL
UPDATE
  job_logs
SET
  status = ?,
  updated_at = ?
WHERE
  type = ?
SQL
        );
        return $stmt->execute([
            $status,
            time(),
            $type
        ]);
    }

    /**
     * ジョブログを作成または更新します。
     * @param $type int ジョブログ種別
     * @param $status int ジョブログステータス
     * @return bool 処理が成功した場合、true。そうでなければfalse。
     */
    public function create_or_update($type, $status)
    {
        if ($this->find($type)) {
            return $this->update($type, $status);
        }
        return $this->create($type, $status);
    }

    /**
     * ジョブログを作成します。
     * @param $type int ジョブログ種別
     * @param $status int ジョブログステータス
     * @return bool 処理が成功した場合、true。そうでなければfalse。
     */
    public function create($type, $status)
    {
        $stmt = $this->conn->prepare(<<<SQL
INSERT INTO
  job_logs
(
  type,
  status,
  updated_at
) VALUES (
  ?,?,?
)
SQL
        );
        return $stmt->execute([
            $type,
            $status,
            time()
        ]);
    }

    /**
     * 動画情報取得ジョブ開始を記録します。
     */
    public function start_video_job()
    {
        $this->create_or_update(JobLogType::VIDEO, JobLogStatus::RUNNING);
    }

    /**
     * 動画情報取得ジョブ異常終了を記録します。
     */
    public function abort_video_job()
    {
        $this->create_or_update(JobLogType::VIDEO, JobLogStatus::ABORTED);
    }

    /**
     * 動画情報取得ジョブ正常終了を記録します。
     */
    public function done_video_job()
    {
        $this->create_or_update(JobLogType::VIDEO, JobLogStatus::DONE);
    }

    /**
     * コメント情報取得ジョブ開始を記録します。
     */
    public function start_comment_job()
    {
        $this->create_or_update(JobLogType::COMMENT, JobLogStatus::RUNNING);
    }

    /**
     * コメント情報取得ジョブ異常終了を記録します。
     */
    public function abort_comment_job()
    {
        $this->create_or_update(JobLogType::COMMENT, JobLogStatus::ABORTED);
    }

    /**
     * コメント情報取得ジョブ正常終了を記録します。
     */
    public function done_comment_job()
    {
        $this->create_or_update(JobLogType::COMMENT, JobLogStatus::DONE);
    }

    public function find($type)
    {
        $stmt = $this->conn->prepare(<<<SQL
SELECT
  status,
  updated_at
FROM
  job_logs
WHERE type = ?
SQL
        );
        $stmt->execute([$type]);
        return $stmt->fetch();
    }

    public function find_video_job_log()
    {
        return $this->find(JobLogType::VIDEO);
    }

    public function find_comment_job_log()
    {
        return $this->find(JobLogType::COMMENT);
    }
}
