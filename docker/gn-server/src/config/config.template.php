<?php

/**
 * Class Config 設定クラス
 */
class Config {
    // DB接続情報
    const DB_HOST = "mysql:host=mysql;dbname=nicoru;charset=utf8mb4";
    const DB_USER = "root";
    const DB_PASS = "password";

    // true の時、ログをファイルに出力します。false の時、ログを出力しません。
    const LOG_TO_FILE = false;

    // true の時、エラーを表示します。
    const SHOW_ERROR = false;

    // niconico ログイン情報
    const NICONICO_ID = ""; // ID
    const NICONICO_PW = ""; // パスワード

    // メール設定
    const MAIL_HOST = ""; // SMTPサーバ
    const MAIL_FROM = ""; // 送信元
    const MAIL_TO = ""; // 送信先
    const MAIL_PASSWORD = ""; // サーバログインパスワード
}
