<?php
/**
 * ユーティリティモジュール
 */

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once("/usr/local/work/vendor/phpmailer/phpmailer/src/Exception.php");
require_once("/usr/local/work/vendor/phpmailer/phpmailer/src/PHPMailer.php");
require_once("/usr/local/work/vendor/phpmailer/phpmailer/src/SMTP.php");
require_once(__DIR__ . "/config/config.php");
require_once(__DIR__ . "/niconicoAPIUtil.class.php");
require_once(__DIR__ . "/videoAPIInfo.class.php");
require_once(__DIR__ . "/videoInfo.class.php");
require_once(__DIR__ . "/nikorareDAO.class.php");

date_default_timezone_set("Asia/Tokyo");
if (Config::SHOW_ERROR === true) {
    error_reporting(E_ALL);
} else {
    ini_set('display_errors', 0);
}

/**
 * NIMJを取得する
 * @param $mid string 動画ID
 * @return array NIMJ
 */
function getNimj($mid)
{
    // DBからデータ取得
    $dao = new NikorareDAO();
    $result = $dao->getNikorareForDisplay($mid);

    // 取得したデータの整形
    $resArray = [];
    foreach ($result as $row) {
        $resArray[$row['cid']] = (int)$row['n'];
    }
    return $resArray;
}

/**
 * 特定のコメントのニコられ回数をカウントアップする
 * @param $mid string 動画ID
 * @param $cid string コメントID
 * @param $commented_at string コメントの書込時刻
 * @param $commented_point string コメントの再生時間
 * @return array 更新結果を示すオブジェクト
 *          更新成功の場合
 *              result: "success"（固定値）
 *          更新失敗の場合
 *              error: エラーメッセージ
 */
function nicoru($mid, $cid, $commented_at = null, $commented_point = null)
{
    if ($cid != (int)$cid) return ["error" => "invalid_comment_id"];

    my_log("10001 -- $commented_at, $commented_point");

    if (!commented_at_is_valid($commented_at)) {
        $commented_at = null;
    }

    if (!commented_point_is_valid($commented_point)) {
        $commented_point = null;
    }

    // ニコられ情報を取得する
    $dao = new NikorareDAO();
    $record = $dao->getNikorareForUpdate($mid, $cid);

    if ($record === false) {
        // まだニコられ情報が存在しない場合、新規登録
        my_log("10002-1");

        if (!VideoInfo::getVideoInfo($mid)) {
            // 指定された動画が存在しない場合
            return ["error" => "video_not_found"];
        }
        my_log("10002-2 -- $commented_at, $commented_point, $mid, $cid");
        $dao->create($commented_at, $commented_point, $mid, $cid);
        return ["result" => "success"];
    }
    my_log("10003-1");

    # 未セットの場合は入力値をセットする
    $_commented_at = is_null($record->commented_at) ? $commented_at : $record->commented_at;
    $_commented_point = is_null($record->commented_point) ? $commented_point : $record->commented_point;

    // ニコられ数を更新
    my_log("10003-2 -- $commented_at, $commented_point, $mid, $cid");
    $dao->update($_commented_at, $_commented_point, $mid, $cid);
    return ["result" => "success"];
}

/**
 * ログインデータを取得する
 * @return array 下記の構造を持つログインデータ配列
 *          第一要素：メールアドレス
 *          第二要素：パスワード
 */
function getLoginData()
{
    // ログインデータ
    return array(

        // メールアドレス
        "mail_tel" => Config::NICONICO_ID,

        // パスワード
        "password" => Config::NICONICO_PW
    );
}

/**
 * ハイライト付きでダンプ出力する
 * @param $var mixed 出力する変数
 * @param $index mixed 目印
 */
function hdump($var, $index = 0)
{
    highlight_string("\n" . $index . "<?php\n" . var_export($var, true));
}

/**
 * クライアントが gzip 圧縮に対応していれば、レスポンスを圧縮する
 * @param $response mixed レスポンス
 * @return mixed 圧縮された、もしくはそのままのレスポンス
 */
function compressResponseIfCan($response)
{
    if (clientCanAcceptGzip()) {
        header('Content-Encoding: gzip');
        return gzencode($response, 9);
    }
    return $response;
}

/**
 * クライアントが gzip 圧縮に対応していれば true を返却する。そうでなければ false を返却する。
 * @return bool 判定結果
 */
function clientCanAcceptGzip()
{
    return strpos($_SERVER['HTTP_ACCEPT_ENCODING'], strtolower('gzip')) !== false;
}

/**
 * 秒数を[mm:ss]形式で表現する
 * @param $seconds int 秒数
 * @return string [mm:ss]形式文字列
 */
function s2h($seconds)
{
    $minutes = floor($seconds / 60);
    $seconds = $seconds % 60;
    $hms = sprintf("%02d:%02d", $minutes, $seconds);
    return $hms;
}

/**
 * [mm:ss]形式の秒数から分を取得する
 */
function video_time_point_to_minute($video_time_point)
{
    preg_match('/^(\d+):\d+$/', $video_time_point, $m);
    if (!$m) {
        return null;
    } else {
        return (int)$m[1];
    }
}

/**
 * 書込時刻を検証する
 * @param $commented_at string クライアントから送信されたコメント書込時刻
 * @return bool 検証結果
 */
function commented_at_is_valid($commented_at)
{
    try {
        $time_string = strtotime($commented_at);
        $result = date('Y/m/d', $time_string);
        if ($result === $commented_at) {
            return true;
        }
        return false;
    } catch (Exception $e) {
        return false;
    }
}

/**
 * 再生時間を検証する
 * @param $commented_point string 再生時間
 * @return bool 検証結果
 */
function commented_point_is_valid($commented_point)
{
    try {
        preg_match('/^\d+:\d+$/', $commented_point, $m);
        if ($m) {
            return true;
        }
        return false;
    } catch (Exception $e) {
        return false;
    }
}

/**
 * ログにメッセージを出力する
 * @param $message string メッセージ
 */
function my_log($message)
{
    if (Config::LOG_TO_FILE) {
        error_log("$message\n", 3, '/var/tmp/app.log');
    }
}

/**
 * 基本的なヘッダをセットする
 */
function setHeader()
{
    header('Content-type: application/json');
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
    header_remove('X-Powered-By');
}

/**
 * メールを送信する
 * @param $subject string タイトル
 * @param $message string 本文
 */
function send_mail($subject, $body)
{
    $mail = new PHPMailer(true); // Passing `true` enables exceptions
    try {
        //Server settings
//        $mail->SMTPDebug = 4; // Enable verbose debug output
        $mail->isSMTP(); // Set mailer to use SMTP
        $mail->Host = Config::MAIL_HOST; // Specify main and backup SMTP servers
        $mail->SMTPAuth = true; // Enable SMTP authentication
        $mail->Username = Config::MAIL_FROM; // SMTP username
        $mail->Password = Config::MAIL_PASSWORD; // SMTP password
//        $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587; // TCP port to connect to

        //Recipients
        $mail->setFrom(Config::MAIL_FROM, 'GijiNikoru');
        $mail->addAddress(Config::MAIL_TO, 'GijiNikoru Admin'); // Add a recipient
        $mail->addReplyTo(Config::MAIL_FROM, 'Information');

        //Content
        $mail->Subject = $subject;
        $mail->Body = $body;

        $mail->send();
        echo 'Message has been sent';
    } catch (Exception $e) {
        echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    }
}

/**
 * HTML特殊文字をエスケープする関数
 *
 * @param $str string 対象の文字列
 * @return string 処理された文字列
 */
function h($str)
{
    if (contain($str, "<") || contain($str, ">")) {
        // <, > を含む場合、エスケープする
        return htmlspecialchars($str, ENT_QUOTES, 'UTF-8');
    }
    // 上記以外の場合、XSSに使用される文字はエスケープ済みと判断して、エスケープを行わない（二重エスケープを避けるため）。
    // NOTE: 2018/01/23 現在、niconico から取得した文字列はエスケープされている。
    return $str;
}

/**
 * h関数の逆を行う関数
 *
 * @param $str string 対象の文字列
 * @return string 処理された文字列
 */
function h_decode($str)
{
    return htmlspecialchars_decode($str, ENT_QUOTES);
}

/**
 * 文字列Aが文字列Bを含むかどうかを検査します。
 *
 * @param $a string 文字列A
 * @param $b string 文字列B
 * @return boolean 文字列Aが文字列Bを含む場合、trueを返却します。含まない場合、falseを返却します。
 */
function contain($a, $b)
{
    return strpos($a, $b) !== false;
}
