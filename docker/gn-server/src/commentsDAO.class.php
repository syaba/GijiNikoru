<?php

require_once(__DIR__ . "/utils.php");
require_once(__DIR__ . "/dbUtils.php");

/**
 * Class CommentsDAO comments テーブル用 DAO クラス
 */
class CommentsDAO
{
    // DB接続
    private $conn;

    public function __construct()
    {
        // 接続する
        try {
            $this->conn = getDatabaseConnection();
        } catch (PDOException $e) {
            return ["error" => "db_connect_failed", "description" => $e->getMessage()];
        }
    }

    public function create($video_id, Comment $comment)
    {
        $stmt = $this->conn->prepare(<<<SQL
INSERT INTO
  comments
(
  video_id,
  id,
  text,
  posted_at,
  posted_by,
  point,
  original_nicorare,
  was_deleted
) VALUES (
  ?,?,?,?,?,?,?,?
)
SQL
        );
        $stmt->execute([
            $video_id,
            $comment->id,
            $comment->text,
            $comment->posted_at,
            $comment->posted_by,
            $comment->point,
            $comment->old_nikorare,
            $comment->was_deleted
        ]);
    }

//    public function find($video_id, $id)
//    {
//        $stmt = $this->conn->prepare(<<<SQL
//SELECT
//  video_id,
//  id,
//  text,
//  posted_at,
//  point,
//  original_nicorare,
//  updated_at
//FROM
//  comments
//WHERE movie_id = ?
//AND id = ?
//SQL
//        );
//        $stmt->execute([$video_id, $id]);
//        return $stmt->fetch();
//    }
}
