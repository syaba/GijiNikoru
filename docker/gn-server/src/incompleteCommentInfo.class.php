<?php

/**
 * Class IncompleteCommentInfo 不完全コメント情報クラス
 */
class IncompleteCommentInfo
{
    public $video_id;
    public $comment_ids;

    public function __construct($video_id, $comment_ids)
    {
        $this->video_id = $video_id;
        $this->comment_ids = $comment_ids;
    }
}
