<?php
/**
 * ユーティリティモジュール
 */

// 設定ファイルを読み込む
require_once(__DIR__ . "/config/config.php");

// phpバージョンを隠蔽する
header_remove('X-Powered-By');

/**
 * DB接続を取得する
 * @return PDO PDOインスタンス
 */
function getDatabaseConnection()
{
    return new PDO(Config::DB_HOST, Config::DB_USER, Config::DB_PASS);
}
