<?php

require_once(__DIR__ . "/comments.class.php");

/**
 * Class NiconicoAPIUtil niconico API用ユーティリティクラス
 */
class NiconicoAPIConnector
{
    public $login_cookie_tmp_file;
    public $login_cookie_tmp_file_fp;

    public function __construct()
    {
        $this->login_cookie_tmp_file_fp = tmpfile();
        $this->login_cookie_tmp_file = stream_get_meta_data($this->login_cookie_tmp_file_fp);
        self::login($this->login_cookie_tmp_file);
    }

    public function __destruct()
    {
        fclose($this->login_cookie_tmp_file_fp);
    }

    /**
     * 動画情報を取得する
     * @param $video_id string 動画ID
     * @return VideoAPIInfo 動画情報
     */
    public function getVideoAPIInfo($video_id)
    {
        $result = NiconicoAPIConnector::accessToAPI("http://flapi.nicovideo.jp/api/getflv/$video_id",
            $this->login_cookie_tmp_file);
        parse_str($result, $parsed_result);
        return new VideoAPIInfo($video_id, $parsed_result);
    }

    /**
     * コメントを取得する
     * @param $video_id string 動画ID
     * @param $commented_at int コメントの「書込時刻」（UNIX秒）
     * @param $commented_point string コメントの「再生時間」（mm:ss 形式）
     * @return Comments コメント集合
     */
    public function getComments($video_id, $commented_at = null, $commented_point = null, $channel = false)
    {
        $video_api_info = $this->getVideoAPIInfo($video_id);
        $params_and_condition = self::getParamsForComment($video_api_info, $commented_at, $commented_point, $channel);
        $result = self::accessToAPI(
            $video_api_info->comment_server_url_json,
            $this->login_cookie_tmp_file,
            true,
            json_encode([$params_and_condition["params"]]),
            true);

        return new Comments($params_and_condition["condition"], json_decode($result, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
    }

    public function postComment($url, $thread, $vpos, $mail, $ticket, $user_id, $content, $post_key)
    {
        $param = json_encode(array(
            "chat" => array(
                "thread" => $thread,
                "vpos" => $vpos,
                "mail" => $mail,
                "ticket" => $ticket,
                "user_id" => $user_id,
                "premium" => 1,
                "content" => html_entity_decode($content),
                "postkey" => $post_key,
            )
        ));
        $result = self::accessToAPI(
            $url,
            $this->login_cookie_tmp_file,
            true,
            $param,
            true
        );
        return json_decode($result, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }

    private function getCommentedPoint(VideoInfo $video_info, $commented_point = null)
    {
        if (is_null($commented_point)) {
            // 指定がない場合は動画時間を利用する
            return "0-" . video_time_point_to_minute($video_info->length);
        }
        $commented_minute = video_time_point_to_minute($commented_point);
        if (is_null($commented_minute)) {
            // 指定があるが形式が不正な場合
            my_log("30001 -- $commented_point");
            return "0-100";
        }

        // 指定があり、形式も正常な場合
        return "${commented_minute}-${commented_minute}";
    }

    private function getParamsForComment(VideoAPIInfo $video_api_info, $commented_at, $commented_point, $channel = false)
    {
        $wayback_key = $this->getWaybackKey($video_api_info);
        $thread_key = $this->getThreadKey($video_api_info);

        if (is_null($commented_at)) {
            $commented_at = time();
        } else {
            // 指定がある場合は指定時刻から1日後
            $commented_at += 60 * 60 * 24;
        }
        $video_info = VideoInfo::getVideoInfo($video_api_info->video_id);
        $commented_point = $this->getCommentedPoint($video_info, $commented_point);
        $condition = array(
            "video_info" => $video_info,
            "commented_point" => $commented_point,
            "commented_at" => $commented_at,
        );
        $params = array(
            "thread" => array(
                "thread" => $video_api_info->thread_id,
                "language" => 0,
                "scores" => 1,
                "nicoru" => 1,
                "user_id" => $video_api_info->user_id,
                "waybackkey" => $wayback_key,
                "force_184" => $thread_key["force_184"],
                "threadkey" => $thread_key["threadkey"],
                "version" => 20090904,
                "with_global" => 1,
            ),
            "thread_leaves" => array(
                "thread" => $video_api_info->thread_id,
                "language" => 0,
                "scores" => 1,
                "nicoru" => 1,
                "user_id" => $video_api_info->user_id,
                "waybackkey" => $wayback_key,
                "force_184" => $thread_key["force_184"],
                "threadkey" => $thread_key["threadkey"],
                "content" => "${commented_point}:1000,2000", // [取得開始時点（分）]-[取得終了時点（分）]:[???],[???]
            )
        );

        if (!is_null($commented_at)) {
            // 投稿時刻の条件指定がある場合、これを加える
            $params["thread"]["when"] = $commented_at;
            $params["thread_leaves"]["when"] = $commented_at;
        }
        return array(
            "condition" => $condition,
            "params" => $params,
        );
    }

    /**
     * niconicoにログインする
     * @param $login_cookie_tmp_file array ログインクッキー一時ファイル
     */
    private static function login($login_cookie_tmp_file)
    {
        self::accessToAPI(
            "https://secure.nicovideo.jp/secure/login?site=niconico",
            $login_cookie_tmp_file,
            TRUE,
            getLoginData());
    }

    /**
     * APIにアクセスする
     * @param $url : URL
     * @param $login_cookie_tmp_file : クッキー用一時ファイル
     * @param $is_post : POSTか
     * @param $post_fields : ポストフィールド
     * @return mixed 結果
     */
    private static function accessToAPI($url, $login_cookie_tmp_file, $is_post = true, $post_fields = null, $is_json = false)
    {
        // 初期化
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        // クッキー利用
        curl_setopt($ch, CURLOPT_COOKIEJAR, $login_cookie_tmp_file['uri']);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $login_cookie_tmp_file['uri']);

        if ($is_post) {
            // POSTリクエストの場合
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
        }

        curl_setopt($ch, CURLOPT_HEADER, TRUE);
        if ($is_json) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        }

        // アクセス実行
        $result = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $header = substr($result, 0, $header_size);
        $body = substr($result, $header_size);

        // 接続を閉じる
        curl_close($ch);

        return $body;
    }

    /**
     * スレッドキーを取得する
     * @param $video_info VideoAPIInfo 動画情報
     * @return mixed スレッドキー
     */
    private function getThreadKey(VideoAPIInfo $video_info)
    {
        $result = self::accessToAPI(
            "http://flapi.nicovideo.jp/api/getthreadkey?thread=" . $video_info->thread_id,
            $this->login_cookie_tmp_file
        );
        parse_str($result, $parsed_result);
        return $parsed_result;
    }

    public function getBlockNo($comment_num)
    {
        return (int)(((int)$comment_num + 1) / 100);
    }

    /**
     * コメント投稿キーを取得する
     *
     * レスポンスのステータスについて
     * 0 = SUCCESS(投稿完了)
     * 1 = FAILURE(投稿拒否)
     * 2 = INVALID_THREAD(スレッドIDがおかしい)
     * 3 = INVALID_TICKET(投稿チケットが違う)
     * 4 = INVALID_POSTKEY(ポストキーがおかしい or ユーザーIDがおかしい)
     * 5 = LOCKED(コメントはブロックされている)
     * 6 = READONLY(コメントは書き込めない)
     * 8 = TOO_LONG(コメント内容が長すぎる)
     *
     * @param $video_id string 動画ID
     * @param $thread string スレッド
     * @return mixed コメント投稿キー
     */
    public function getPostKey($block_no, $thread)
    {
        $result = self::accessToAPI(
            "http://flapi.nicovideo.jp/api/getpostkey?thread=${thread}&block_no=${block_no}&device=1&version=1&version_sub=6",
            $this->login_cookie_tmp_file
        );
        parse_str($result, $parsed_result);
        return $parsed_result["postkey"];
    }

    /**
     * Waybackキーを取得する
     * @param $video_info VideoAPIInfo 動画情報
     * @return mixed Waybackキー
     */
    private function getWaybackKey(VideoAPIInfo $video_info)
    {
        $result = self::accessToAPI(
            "http://flapi.nicovideo.jp/api/getwaybackkey?thread=" . $video_info->thread_id,
            $this->login_cookie_tmp_file
        );
        parse_str($result, $parsed_result);
        return $parsed_result['waybackkey'];
    }
}
