<?php

require_once(__DIR__ . "/utils.php");
require_once(__DIR__ . "/dbUtils.php");
require_once(__DIR__ . "/incompleteCommentInfo.class.php");

/**
 * Class NikorareDAO GijiNikoru nikorare テーブル用 DAO クラス
 */
class NikorareDAO
{
    // DB接続
    private $conn;

    public function __construct()
    {
        // 接続する
        try {
            $this->conn = getDatabaseConnection();
        } catch (PDOException $e) {
            return ["error" => "db_connect_failed", "description" => $e->getMessage()];
        }
    }

    public function update($commented_at, $commented_point, $mid, $cid)
    {
        $stmt = $this->conn->prepare("UPDATE nikorare SET nikorare=nikorare+1, commented_at=?, commented_point=? WHERE movie_id = ? AND comment_id = ?");
        $stmt->execute([$commented_at, $commented_point, $mid, $cid]);
    }

    public function create($commented_at, $commented_point, $mid, $cid)
    {
        $stmt = $this->conn->prepare("INSERT INTO nikorare(movie_id, comment_id, nikorare, commented_at, commented_point) VALUES (?,?,?,?,?)");
        $stmt->execute([$mid, $cid, 1, $commented_at, $commented_point]);
    }

    public function getNikorareForUpdate($mid, $cid)
    {
        $stmt = $this->conn->prepare("SELECT comment_id AS cid, nikorare, commented_at, commented_point AS n FROM nikorare WHERE movie_id = ? AND comment_id = ?");
        $stmt->execute([$mid, $cid]);
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function getNikorareForDisplay($mid)
    {
        $stmt = $this->conn->prepare("SELECT comment_id AS cid, nikorare AS n FROM nikorare WHERE movie_id = ?");
        $stmt->execute([$mid]);
        return $stmt->fetchAll();
    }

    /**
     * 補完情報が得られていない動画の動画IDを返却する
     * @return mixed 結果
     */
    public function getIncompleteVideoInfo()
    {
        $stmt = $this->conn->prepare(<<<SQL
SELECT
  movie_id
FROM
  nikorare n
WHERE NOT EXISTS (
  SELECT
    *
  FROM
    videos v
  WHERE
    v.id = n.movie_id
)
SQL
        );
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$result) {
            return null;
        }
        return $result['movie_id'];
    }

    /**
     * 補完情報が得られていないコメントの動画IDとコメントIDを返却する
     * @return array 結果
     */
    public function getIncompleteCommentInfo()
    {
        // 補完情報が無いもののうちコメント最多の動画IDを取得する。
        $stmt = $this->conn->prepare(file_get_contents(__DIR__ . '/sql/GET_INCOMPLETE_COMMENT_INFO.sql'));
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }


    /**
     * ニコられ情報を返却する
     * @param $user_id string ユーザID
     * @return array 結果
     */
    public function getNikorareInfo($user_id)
    {
        $stmt = $this->conn->prepare(file_get_contents(__DIR__ . '/sql/GET_NIKORARE_INFO.sql'));
        $stmt->execute([$user_id]);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

}
