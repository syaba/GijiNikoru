<?php

require_once(__DIR__ . "/utils.php");
require_once(__DIR__ . "/dbUtils.php");
require_once(__DIR__ . "/videoInfo.class.php");
require_once(__DIR__ . "/videoAPIInfo.class.php");

/**
 * Class VideosDAO videos テーブル用 DAO クラス
 */
class VideosDAO
{
    // DB接続
    private $conn;

    public function __construct()
    {
        // 接続する
        try {
            $this->conn = getDatabaseConnection();
        } catch (PDOException $e) {
            return ["error" => "db_connect_failed", "description" => $e->getMessage()];
        }
    }

    public function create(VideoAPIInfo $vai, VideoInfo $vi)
    {
        $stmt = $this->conn->prepare(<<<SQL
INSERT INTO
  videos
(
  id,
  thumbnail,
  posted_at,
  length,
  title,
  watch_url,
  posted_by,
  posted_by_name
) VALUES (
  ?,?,?,?,?,?,?,?
)
SQL
        );
        return $stmt->execute([
            $vi->video_id,
            $vi->thumbnail_url,
            $vai->posted_at,
            $vi->length,
            $vi->title,
            $vi->watch_url,
            $vi->author_user_id,
            $vi->author_nickname,
        ]);
    }

    public function find($id)
    {
        $stmt = $this->conn->prepare(<<<SQL
SELECT
  id,
  thumbnail,
  posted_at,
  length,
  updated_at
FROM
  videos
WHERE id = ?
SQL
        );
        $stmt->execute([$id]);
        return $stmt->fetch();
    }
}
