<?php

require_once(__DIR__ . "/../src/utils.php");

Class ValidateCommentedAtTest extends PHPUnit_Framework_TestCase
{
    public function test_valid_case()
    {
        $commented_at = "2017/10/10";
        $result = commented_at_is_valid($commented_at);
        $this->assertTrue($result);
    }

    public function test_invalid_case()
    {
        $commented_at = "2017/1010";
        $result = commented_at_is_valid($commented_at);
        $this->assertFalse($result);

        $commented_at = "2017/10/00";
        $result = commented_at_is_valid($commented_at);
        $this->assertFalse($result);

        $commented_at = null;
        $result = commented_at_is_valid($commented_at);
        $this->assertFalse($result);

        $commented_at = "";
        $result = commented_at_is_valid($commented_at);
        $this->assertFalse($result);

        $commented_at = "abc";
        $result = commented_at_is_valid($commented_at);
        $this->assertFalse($result);
    }
}

Class ValidateCommentedPointTest extends PHPUnit_Framework_TestCase
{
    public function test_valid_case()
    {
        $this->assertTrue(commented_point_is_valid("12:34"));
        $this->assertTrue(commented_point_is_valid("0:34"));
        $this->assertTrue(commented_point_is_valid("120:34"));
    }

    public function test_invalid_case()
    {
        $this->assertFalse(commented_point_is_valid(":34"));
        $this->assertFalse(commented_point_is_valid("0:"));
        $this->assertFalse(commented_point_is_valid(":"));
        $this->assertFalse(commented_point_is_valid("1a1:2"));
        $this->assertFalse(commented_point_is_valid("11:2a4"));
        $this->assertFalse(commented_point_is_valid("11;23"));
        $this->assertFalse(commented_point_is_valid("abc"));
        $this->assertFalse(commented_point_is_valid(""));
        $this->assertFalse(commented_point_is_valid(null));
    }
}
