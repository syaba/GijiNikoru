USE nicoru;
CREATE TABLE videos (
  id       VARCHAR(100) NOT NULL,
  title          VARCHAR(300),
  thumbnail      VARCHAR(200) NOT NULL,
  posted_at      DATETIME,
  length         VARCHAR(10),
  watch_url      VARCHAR(300),
  posted_by      VARCHAR(30),
  posted_by_name VARCHAR(200),
  updated_at     TIMESTAMP    NOT NULL DEFAULT current_timestamp ON UPDATE current_timestamp,
  PRIMARY KEY (id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;
