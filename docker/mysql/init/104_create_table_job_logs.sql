USE nicoru;
CREATE TABLE job_logs (
  type       TINYINT         NOT NULL,
  status     TINYINT         NOT NULL,
  updated_at BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (type)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;
