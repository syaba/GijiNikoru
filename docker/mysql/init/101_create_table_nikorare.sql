USE nicoru;
CREATE TABLE nikorare (
  movie_id        VARCHAR(100) NOT NULL,
  comment_id      VARCHAR(100) NOT NULL,
  nikorare        INT(30)      NOT NULL,
  commented_at    VARCHAR(20),
  commented_point VARCHAR(10),
  PRIMARY KEY (movie_id, comment_id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;
