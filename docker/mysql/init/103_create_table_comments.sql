USE nicoru;
CREATE TABLE comments (
  video_id          VARCHAR(100) NOT NULL,
  id                VARCHAR(100) NOT NULL,
  text              VARCHAR(300),
  posted_at         DATETIME,
  posted_by         VARCHAR(100),
  point             VARCHAR(10),
  was_deleted       BOOLEAN,
  original_nicorare SMALLINT UNSIGNED,
  updated_at        TIMESTAMP    NOT NULL DEFAULT current_timestamp ON UPDATE current_timestamp,
  PRIMARY KEY (video_id, id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;
